package talib

import com.tictactec.ta.lib.{MInteger, Core, MAType}
import models.{Types, Stock, Conversions, StockItem}

/**
 * Little wrapper for the unhandy functions.
 *
 * We define: If a measure is calculated based on a timeline, the first element in an array is that value that is most lying in the past.
 * The last value is for the latest day
 * http://www.ta-lib.org/d_api/d_api.html
 */
object TaLib {
  private val core = new Core()

  /**
   * A average calculates a average stock price for the last x days. So a average will return one stock price.
   * A moving average does the same but calculates a average for every day.
   * E.g.: we start 10 days ago and calculate a moving average over the last 15 days. Imagine that now we have the 30th of January.
   * The first average is calculated for the 20th of january and averages the days from the 5th to the 20th.
   * The second average is calculated for the 21th of january and averages the days from the 6th to the 20th.
   * .... and so on
   * At the end we will end up with 10 different averages. Drawn onto a stock chart, this average prices can give a hint where the stock price will go.
   *
   * Using a moving average, one can smooth a time line. Short leaps are filtered out.
   * This can be used to determine a trend without loosing sight for the essential.
   *
   * Moving averages are good during trend phases but loose their significance in sideways markets.
   */
  object MovingAverage {

    def simpleMovingAverage(averageOverLastDays: Int, averageForDays: Int, items: Seq[StockItem], filter: (Seq[StockItem]) => Array[Double]): Array[Double] = {
      require(items.size >= averageOverLastDays + averageForDays - 1, s"there aren't enough items available to calculate the desired averages. Available: ${items.size}, required: ${averageForDays - 1 + averageForDays}")

      val closePrices = filter(items).take(averageForDays + averageOverLastDays)
      val output = new Array[Double](averageForDays + averageOverLastDays)
      val outputBegin = new MInteger()
      val outNbElement = new MInteger()

      core.movingAverage(0, closePrices.size - 1, closePrices, averageOverLastDays, MAType.Sma, outputBegin, outNbElement, output)

      // the first average is for the most lying in the past
      output.take(averageForDays).reverse
    }
  }

  /**
   * An oscillator measures the momentum of a market.
   * That means that it shows a flattening of a trend.
   *
   * Example:
   * |Day|  1,   2,    3,  4,    5
   * |Price|10, 11, 11.5, 12, 11.5
   *
   * An oscillator will show a little momentum within an up trend.
   *
   * Example 2:
   * |Day|  1,   2, 3, 4
   * |Price|10,  8, 4, 1
   *
   * Here we have a down trend with a high momentum.
   * The oscillator will show a increasing bearish momentum
   *
   * An oscillator can be used as an early warning signal for changing trends.
   */
  object Oscillator {

    /**
     * SignalTypes reduce the absolute values (which can have any number) to 5 events.
     */
    object SignalTypes {

      abstract class SignalType(val id:Int)

      case class BullishMomentumUp() extends SignalType(2)

      case class BullishMomentumDown() extends SignalType(1)

      case class BearishMomentumUp() extends SignalType(-2)

      case class BearishMomentumDown() extends SignalType(-1)

      case class Neutral() extends SignalType(0)
    }

    /**
     * The transformer is used to overload the oscillator function based on the return type of the transform function.
     * Thus we can use the same method to either return double values or SignalTypes
     */
    object Transformer {
      def defaultArray(items: Array[Double]): Array[Double] = items

      def arrayToSignalType(items: Array[Double]): Array[SignalTypes.SignalType] = {
        items.sliding(2).toArray.map{ x:Array[Double] =>
          val (last, current) = (x(0), x(1))
          (last, current) match {
            case _ if last == current => SignalTypes.Neutral()
            case _ if last >= current && current > 0 => SignalTypes.BullishMomentumDown()
            case _ if last < current && current > 0 => SignalTypes.BullishMomentumUp()
            case _ if last >= current && current < 0 => SignalTypes.BearishMomentumDown()
            case _ if last < current && current < 0 => SignalTypes.BearishMomentumUp()
          }
        }
      }
    }

    def doubleMovingAverage[T](movingAverage: (Int, Int, Seq[StockItem], (Seq[StockItem]) => Array[Double]) => Array[Double]
                                     , maQuickOverLastDays: Int, maSlowOverLastDays: Int, forDays: Int
                                     , items: Seq[StockItem], filter: (Seq[StockItem]) => Array[Double], transform: Array[Double] => T): T = {
      require(items.size >= forDays - 1 + maSlowOverLastDays, s"not enough items available. Required: ${forDays - 1 + maSlowOverLastDays}, passed: ${items.size}")

      val maQuickPrices = movingAverage(maQuickOverLastDays, forDays, items, filter)
      val maSlowPrices = movingAverage(maSlowOverLastDays, forDays, items, filter)
      transform(maQuickPrices.zipWithIndex.map{ case (price, index) => price - maSlowPrices(index)})
    }
  }

}
