package adapter

import java.nio.file.{Path, Files}
import java.nio.charset.Charset
import java.io.File

abstract class DataAdapter(symbol:String) {
  def getStockPriceLine:Seq[String]
}
