package adapter

import models.StockItem
import models.Conversions._

class StockAdapter(dataAdapter:DataAdapter) {
  def getStockPrices:Seq[StockItem] = {
    dataAdapter.getStockPriceLine.map{line =>
      val splits = line.split(",")
      StockItem(splits(0), splits(1), splits(2), splits(3), splits(4), splits(5))
    }
  }
}
