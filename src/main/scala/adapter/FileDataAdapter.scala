package adapter

import java.nio.file.Files
import java.io.File
import java.nio.charset.Charset

class FileDataAdapter(symbol:String) extends DataAdapter(symbol) {
  val path = s"src/main/ressources/$symbol.csv"

  def getStockPriceLine:Seq[String] = {
    import scala.collection.JavaConversions._
    Files.readAllLines(new File(path).toPath, Charset.forName("utf-8")).zipWithIndex.filter(_._2 > 0).unzip._1
  }
}
