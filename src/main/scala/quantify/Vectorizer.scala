package quantify

import models.{Types, StockItemTradeSignal, TradeSignal, StockItem}
import talib.TaLib
import java.util.Date
import java.io.{FileWriter, BufferedOutputStream, FileOutputStream}
import models.TradeSignal.NotBuy

object Vectorizer {
  /**
   * Berechne alles was in TaLib enthalten ist.
   * EIn DenseVector berechnet die Werte einschließlich Date
   *
   * @param items
   * @param mapper
   * @return
   */
  def vectorize(items: Seq[StockItem], mapper: (Seq[StockItem]) => Array[Double], timePeriods: Int = 600): List[(StockItem, DenseVector)] = {
    // there should be enough items
    // generate at least vectors for 600 time periods
    val itemsSorted = items.toArray.sortBy(-_.date.getTime).take(timePeriods).reverse
    val movingAverages = TaLib.MovingAverage.simpleMovingAverage(100, 600, items, mapper)
    val dmaDoubles = TaLib.Oscillator.doubleMovingAverage(TaLib.MovingAverage.simpleMovingAverage, 15, 50, 600, items, mapper, TaLib.Oscillator.Transformer.defaultArray)
    val dmaSignal = TaLib.Oscillator.doubleMovingAverage(TaLib.MovingAverage.simpleMovingAverage, 15, 50, 601, items, mapper, TaLib.Oscillator.Transformer.arrayToSignalType)

    (0 until timePeriods).map {
      i =>
        (itemsSorted(i), new DenseVector(Array(movingAverages(i), dmaDoubles(i), dmaSignal(i).id))) // dmaSignal hat ein element weniger, da immer zwei Elemente verglichen werden
    }.toList
  }

  def toFile(output:String, list: List[(TradeSignal, Date, DenseVector)]) {
    val out = new FileWriter(output)
    val strings = list.sortBy(_._2.getTime).map {
      case (tradeSignal, date, denseVector) =>
        s"${tradeSignal.id} ${denseVector.values.mkString(" ")}\n"
    }
    strings.foreach(out.write)
    out.flush()
  }

  def combineTradeSignalsWithVector(signals: Seq[StockItemTradeSignal], items:Seq[StockItem], vectors:List[(StockItem, DenseVector)]):List[(TradeSignal, Date, DenseVector)] = {
    signals.map( x => {
      val v = vectors.find(_._1.date.getTime() == x.stockItem.date.getTime())
      v match {
        case None => {
          (NotBuy(), null, null)
        }
        case v => {
          (x.tradeSignal, v.get._1.date, v.get._2)
        }
      }
    }).toList
  }
}
