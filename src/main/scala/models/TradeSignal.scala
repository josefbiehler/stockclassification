package models

abstract class TradeSignal(val id:Int = 0)

/**
 * Ein TradeSignal drückt aus, ob ein Produkt gekauft werden kann oder nicht.
 * Direkte Verkaufsempfehlungen sind nicht vorgesehen, da dies ein 3-Klassen Problem erzeugen würde.
 *
 * A TradeSignal express whether or not a product can be bought.
 * A sell recommendation is not intended because this may produce a three-class problem.
 */
object TradeSignal {

  case class Buy() extends TradeSignal(1)

  case class NotBuy() extends TradeSignal(0)

}

/**
 * Associates a StockItem with a TradeSignal.
 * @param stockItem
 * @param tradeSignal
 */
case class StockItemTradeSignal(stockItem: StockItem, tradeSignal: TradeSignal)

object StockItemTradeSignal {
  /**
   * Ein Kaufsignal wird dann erzeugt, wenn ausgehend von Tag X0 innerhalb der nächsten X Tage der Kurs einen gewissen Prozentsatz im vergleich zum Tag X0 gestiegen ist.
   * Dazu wird ein "sliding window" erzeugt. X0 ist dann stehts das erste Element dieses Fensters.
   * Die Suche beschränkt sich darauf, ob überhaupt einmal im Window ein Kurs dabei ist, der entsprechend höher ist, als der erste Kurs des Fensters.
   * @param mapper
   * @param items
   * @param windowSize
   * @param minPercentagePriceDifference
   * @return
   */
  def associateStockItemsWithTradeSignals(mapper: (Seq[StockItem]) => Array[Double], itemsFull: Seq[StockItem], windowSize: Int, minPercentagePriceDifference: Double, newestN:Int): Seq[StockItemTradeSignal] = {
    val items = itemsFull.sortBy(-_.date.getTime).take(newestN)

    items.sortBy(_.date.getTime).sliding(windowSize)
      .map {
      case window =>
        val firstPrice = mapper(Seq(window(0)))(0) // mapper takes a sequence
        // look if any item contains a price that is >= (1 + minPercentagePriceDifference) * firstPrice
        mapper(window).exists(value => value >= (1 + minPercentagePriceDifference) * firstPrice) match {
          case true => StockItemTradeSignal(window(0), TradeSignal.Buy())
          case false => StockItemTradeSignal(window(0), TradeSignal.NotBuy())
        }
    }.toSeq
  }
}