package models

import java.util.Date
import Types._

case class Stock(symbol:String)

case class StockItem(date:Date, open:Open, high:High, low:Low, close:Close, volume:Volume)

object Types {
  type Open = Double
  type High = Double
  type Low = Double
  type Close = Double
  type Volume = Double

  object Mapper {
    def high(items:Seq[StockItem]):Array[Double] = {
      sort(items).map(_.high).reverse
    }
    def open(items:Seq[StockItem]):Array[Double] = {
      sort(items).map(_.open).reverse
    }
    def low(items:Seq[StockItem]):Array[Double] = {
      sort(items).map(_.low).reverse
    }
    def close(items:Seq[StockItem]):Array[Double] = {
      sort(items).map(_.close).reverse
    }
    def volume(items:Seq[StockItem]):Array[Double] = {
      sort(items).map(_.volume).reverse
    }
    private def sort(items:Seq[StockItem]):Array[StockItem] = {
      items.toArray.sortBy(_.date.getTime)
    }
  }
}