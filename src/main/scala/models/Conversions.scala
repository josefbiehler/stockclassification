package models

import java.text.SimpleDateFormat
import java.util.Date
import scala.collection.immutable.StringOps

object Conversions {
  implicit def toDate(dateString:String):Date = {
    val simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd")
    simpleDateFormat.parse(dateString)
  }

  implicit def string2Int(iString:String):Int = {
    new scala.collection.immutable.StringOps(iString).toInt
  }

  implicit def string2Double(dString:String):Double = {
    new scala.collection.immutable.StringOps(dString).toDouble
  }
}
