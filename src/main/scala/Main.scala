import adapter._
import models.{StockItemTradeSignal, TradeSignal, Types}
import quantify.Vectorizer

//http://ichart.finance.yahoo.com/table.csv?s=ALV&d=3&e=3&f=2015&g=d&a=8&b=7&c=2010&ignore=.cvs
object Main {
  def main(args:Array[String]) {
    val items = new StockAdapter(new FileDataAdapter("allianz")).getStockPrices
    val vectors = Vectorizer.vectorize(items, Types.Mapper.close)
    val signals = StockItemTradeSignal.associateStockItemsWithTradeSignals(Types.Mapper.close, items, 50, 0.05, 600)
    val combined = Vectorizer.combineTradeSignalsWithVector(signals, items, vectors)
    Vectorizer.toFile("/NoiseCleaning/vectors.txt", combined)
  }
}
