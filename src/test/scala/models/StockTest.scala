package models

import org.scalatest.{Inspectors, fixture, Matchers, GivenWhenThen}
import models.Conversions._
import models.Types.Mapper._

class StockTest extends fixture.FlatSpec with Matchers with GivenWhenThen with Inspectors {
  type FixtureParam = Seq[StockItem]

  def withFixture(test: OneArgTest) = {
    val items = Seq(
      StockItem("2015-02-11", 1, 11, 111, 1111, 11111),
      StockItem("2015-02-12", 2, 22, 222, 2222, 22222),
      StockItem("2015-02-13", 3, 33, 333, 3333, 33333),
      StockItem("2015-02-14", 4, 44, 444, 4444, 44444),
      StockItem("2015-02-15", 5, 55, 555, 5555, 55555),
      StockItem("2015-02-16", 6, 66, 666, 6666, 66666),
      StockItem("2015-02-17", 7, 77, 777, 7777, 77777),
      StockItem("2015-02-18", 8, 88, 888, 8888, 88888),
      StockItem("2015-02-19", 9, 99, 999, 9999, 99999),
      StockItem("2015-02-20", 10, 100, 1000, 10000, 100000)
    )
    try {
      withFixture(test.toNoArgTest(items))
    }
  }

  it should "sort the prices and volumes according to its date" in { items =>
    Given("test items where every item of a day has a higher price then the item the day before")

    When("filter them")
    val filtered = open(items)

    Then("numbers should be sorted")
    val filteredToTest:Array[(Double, Double)] = ((Array(0.0) ++ filtered).zipWithIndex ++ (filtered ++ Array(10000000.0)).zipWithIndex).groupBy(_._2).map(_._2).collect{case x:Array[(Double, Int)] => (x(0)._1, x(1)._1)}.toArray

    forAll(filteredToTest){case (last, current) => last should be < current}
  }

  it should "filter the 'close' prices" in { items =>
    Given("test items")

    When("the items are mapped to their 'close' prices")
    val mappings = close(items)

    Then("only 'close' prices should be contained")
    forAll(mappings){close => close should (be >= (1111.0) and be <= (10000.0))}
  }

  it should "filter the 'open' prices" in { items =>
    Given("test items")

    When("the items are mapped to their 'open' prices")
    val mappings = open(items)

    Then("only 'open' prices should be contained")
    forAll(mappings){open => open should (be >= (1.0) and be <= (10.0))}
  }

  it should "filter the 'high' prices" in { items =>
    Given("test items")

    When("the items are mapped to their 'high' prices")
    val mappings = high(items)

    Then("only 'high' prices should be contained")
    forAll(mappings){high => high should (be >= (11.0) and be <= (100.0))}
  }

  it should "filter the 'low' prices" in { items =>
    Given("test items")

    When("the items are mapped to their 'low' prices")
    val mappings = low(items)

    Then("only 'low' prices should be contained")
    forAll(mappings){low => low should (be >= (111.0) and be <= (1000.0))}
  }

  it should "filter the 'volume'" in { items =>
    Given("test items")

    When("the items are mapped to their 'volume'")
    val mappings = volume(items)

    Then("only 'volume' should be contained")
    forAll(mappings){volume => volume should (be >= (11111.0) and be <= (100000.0))}
  }
}
