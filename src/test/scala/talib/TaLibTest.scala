package talib

import org.scalatest.{GivenWhenThen, Matchers, fixture}
import org.scalatest.Inspectors._
import models.{StockItem, Types}
import models.Conversions._
import models.Types.Mapper._
import talib.TaLib.{MovingAverage, Oscillator}
import talib.TaLib.Oscillator.SignalTypes.SignalType
import talib.TaLib.Oscillator.SignalTypes

class TaLibTest extends fixture.FlatSpec with Matchers with GivenWhenThen {
  type FixtureParam = Seq[StockItem]

  def withFixture(test: OneArgTest) = {
    val items = Seq(
      StockItem("2015-02-11", 1, 11, 111, 1111, 11111),
      StockItem("2015-02-12", 2, 22, 222, 2222, 22222),
      StockItem("2015-02-13", 3, 33, 333, 3333, 33333),
      StockItem("2015-02-14", 4, 44, 444, 4444, 44444),
      StockItem("2015-02-15", 5, 55, 555, 5555, 55555),
      StockItem("2015-02-16", 6, 66, 666, 6666, 66666),
      StockItem("2015-02-17", 7, 77, 777, 7777, 77777),
      StockItem("2015-02-18", 8, 88, 888, 8888, 88888),
      StockItem("2015-02-19", 9, 99, 999, 9999, 99999),
      StockItem("2015-02-20", 10, 100, 1000, 10000, 100000)
    )
    try {
      withFixture(test.toNoArgTest(items))
    }
  }

  "MovingAverage.simpleMovingAverage" should "run" in {
    items =>
      Given("some test items")
      When("calculating a measure")
      Then("no exception should be thrown")
      TaLib.MovingAverage.simpleMovingAverage(8, 2, items, Types.Mapper.high)
  }

  "MovingAverage.simpleMovingAverage" should "do not calculate a moving average if there aren't enough items" in {
    items =>
      Given("10 items")
      When("calculating a moving average for the last two days over the last 10 days")
      Then("an exception should be thrown")
      intercept[java.lang.IllegalArgumentException] {
        TaLib.MovingAverage.simpleMovingAverage(10, 2, items, close)
      }
  }

  "MovingAverage.simpleMovingAverage" should "calculate 2 averages" in {
    items =>
      Given("some items")
      When("calculating a moving average for the current day over the last 8 days")
      Then("exactly two values should be returned")
      TaLib.MovingAverage.simpleMovingAverage(8, 2, items, close).size should be(2)
  }

  "MovingAverage.simpleMovingAverage" should "calculate the averages as expected" in {
    items =>
      Given("some items")
      When("calculating a moving average")
      val averages = TaLib.MovingAverage.simpleMovingAverage(5, 2, items, open)

      Then("the first average should be 7")
      averages(0) should be(7.0)

      And("the second average should be 8")
      averages(1) should be(8.0)
  }

  "Oscillator.Transformer.defaultArray" should "return double values" in { notUsed =>
    Given("10 double values")
    val values = Array(1.0, 3.0, 2.0, 6.0, 4.0)
    When("calling defaultArray")
    val transformed = Oscillator.Transformer.defaultArray(values)
    Then("Array(1.0, 3.0, 2.0, 6.0, 4.0) should be returned")
    transformed should be (Array(1.0, 3.0, 2.0, 6.0, 4.0))
  }

  "Oscillator.Transformer.arrayToSignalType" should "return signal types" in { notUsed =>
    Given("10 double values")
    val values = Array(1.0, 3.0, 2.0, 6.0, 4.0, -1.0)
    When("calling arrayToSignalType")
    val transformed = Oscillator.Transformer.arrayToSignalType(values)
    Then("4 elements should be returned")
    transformed.size should be (5)
    And("Array(BullishMomentumUp, BullishMomentumDown, BullishMomentumUp, BullishMomentumDown, BearishMomentumDown) should be returned")
    transformed should be (Array(SignalTypes.BullishMomentumUp(), SignalTypes.BullishMomentumDown(), SignalTypes.BullishMomentumUp(),
      SignalTypes.BullishMomentumDown(), SignalTypes.BearishMomentumDown()))
  }

  "Oscillator.doubleMovingAverage" should "run" in {
    items =>
      TaLib.Oscillator.doubleMovingAverage(TaLib.MovingAverage.simpleMovingAverage, 5, 7, 2, items, close, TaLib.Oscillator.Transformer.defaultArray)
  }

  "Oscillator.doubleMovingAverage" should "throw an exception if fewer items than required are available" in { items =>
    Given("10 items")
    When("calling doubleMovingAverage with a quick SMA-8 and a slow SMA-10")
    Then("a IllegalArgumentException should be thrown")
    intercept[IllegalArgumentException] {
      TaLib.Oscillator.doubleMovingAverage(TaLib.MovingAverage.simpleMovingAverage, 8, 10, 2, items, close, Oscillator.Transformer.defaultArray)
    }
  }

  "Oscillator.doubleMovingAverage" should "return six values" in { items =>
    Given("10 items")
    When("calling doubleMovingAverage with a quick SMA-3, a slow SMA-5 and forDays=6")
    val values = Oscillator.doubleMovingAverage(MovingAverage.simpleMovingAverage, 3, 5, 6, items, close, Oscillator.Transformer.defaultArray)
    Then("six items should be returned")
    values.size should be (6)
  }

  "Oscillator.doubleMovingAverage" should "return double values" in { items =>
    Given("10 items")
    When("calling doubleMovingAverage with Transformer.default")
    val values = Oscillator.doubleMovingAverage(MovingAverage.simpleMovingAverage, 3, 4, 6, items, close, Oscillator.Transformer.defaultArray)
    Then("only double values should be returned")
    forAll(values){e => e shouldBe a [java.lang.Double]}
  }

  "Oscillator.doubleMovingAverage" should "return signal types" in { items =>
    Given("10 items")
    When("calling doubleMovingAverage with Transformer.default")
    val values = Oscillator.doubleMovingAverage(MovingAverage.simpleMovingAverage, 3, 4, 6, items, close, Oscillator.Transformer.arrayToSignalType)
    Then("only signal types should be returned")
    forAll(values){e => e shouldBe a [SignalType]}
  }

  "Oscillator.doubleMovingAverage" should "return double values as expected" in { items =>
    /*Given("10 items")
    When("calling doubleMovingAverage with Transformer.default, slow SMA-7, quick SMA-5, forDays = 3")
    val values = Oscillator.doubleMovingAverage(MovingAverage.simpleMovingAverage, 5, 7, 3, items, close, Oscillator.Transformer.defaultArray)
    Then("the ")*/
    // TODO: test
  }
}
