package adapter

import org.scalatest.{Matchers, GivenWhenThen, FlatSpec}

class StockAdapterTest extends FlatSpec with GivenWhenThen with Matchers {
  "A StockAdapter" should "return a list of stock prices" in {
    Given("a stock adapter connected to a file data adapter")
    val adp = new StockAdapter(new FileDataAdapter("allianz"))

    When("retrieving stock items")
    val items = adp.getStockPrices

    Then("list should contain more than 100 items")
    items.size should be > 100
  }
}
