package quantify

import org.scalatest.{FlatSpec, Matchers, GivenWhenThen}
import adapter.{StockAdapter, FileDataAdapter}
import models.{StockItemTradeSignal, TradeSignal, Types, Stock}
import models.TradeSignal.NotBuy

/**
 *
 *
 */
class VectorizerTest extends FlatSpec with Matchers with GivenWhenThen{
  it should "" in {
    val items = new StockAdapter(new FileDataAdapter("allianz")).getStockPrices
    val vectors = Vectorizer.vectorize(items, Types.Mapper.close)
    vectors.size should be (600)


    //Vectorizer.toFile(s)
  }
}
